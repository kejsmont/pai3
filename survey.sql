
CREATE DATABASE IF NOT EXISTS survey;
USE survey;

CREATE TABLE `questions` (
  `question_id` int(11) NOT NULL,
  `question_text` varchar(255) NOT NULL,
  `option_a` varchar(255) NOT NULL,
  `option_b` varchar(255) NOT NULL,
  `option_c` varchar(255) NOT NULL,
  `option_d` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


INSERT INTO `questions` (`question_id`, `question_text`, `option_a`, `option_b`, `option_c`, `option_d`) VALUES
(1, 'What is your favorite color?', 'Red', 'Blue', 'Green', 'Yellow'),
(2, 'What is your favorite color?', 'Red', 'Blue', 'Green', 'Yellow'),
(3, 'What is your favorite color?', 'Red', 'Blue', 'Green', 'Yellow'),
(4, 'What is your favorite color?', 'Red', 'Blue', 'Green', 'Yellow');


CREATE TABLE `question_answers` (
  `name` varchar(32) NOT NULL,
  `q1_ans` varchar(1) NOT NULL,
  `q2_ans` varchar(1) NOT NULL,
  `q3_ans` varchar(1) NOT NULL,
  `q4_ans` varchar(1) NOT NULL
)
